<%@ page language="java" import="com.struts.util.Constants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested" %>

<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<logic:notPresent scope="session" name="user">
    <logic:forward name="adLogon"/>   
</logic:notPresent><jsp:useBean id = "user" scope="session" class="com.struts.util.User"/>
<html>
<head>
<title>
  <%=user.getCompanyName()%> - <%=user.getCurrentBranch().getBrBranchCode()%> - <%=user.getUserName()%> - <bean:message key="itemEntry.title"/> 
</title>
<link rel="stylesheet" href="css/styles.css"
      charset="ISO-8859-1" type="text/css">
<script src="js/global.js"></script>

<script Language="JavaScript" type="text/javascript"> 
<!-- Hide from non-JavaScript Browsers 	
function submitForm()
{
      
      disableButtons();      
      enableInputControls();    
            
}


function toggleTB(what){
	if(what.checked){

		document.forms[0].elements["acquisitionCost"].disabled="false";
		document.forms[0].elements["lifeSpan"].disabled="false";
		document.forms[0].elements["fixedAssetAccount"].disabled="false";
		document.forms[0].elements["fixedAssetAccountDescription"].disabled="false";
		document.forms[0].elements["depreciationAccount"].disabled="false";
		document.forms[0].elements["depreciationAccountDescription"].disabled="false";
	}
}


function recalculatePrice(name) {

	  var unitCost = 0;
	  var averageCost = 0;
	  var shippingCost = 0;
	  var percentMarkup = 0;
	  var salesPrice = 0;
	  var grossProfit = 0;
	  
	  if (!isNaN(parseFloat(document.forms[0].elements["unitCost"].value))) {
	      unitCost = (document.forms[0].elements["unitCost"].value).replace(/,/g,'') * 1;
	  }
	  if (!isNaN(parseFloat(document.forms[0].elements["averageCost"].value))) {
	      averageCost = (document.forms[0].elements["averageCost"].value).replace(/,/g,'') * 1;
	  }
	  if (!isNaN(parseFloat(document.forms[0].elements["shippingCost"].value))) {
	      shippingCost = (document.forms[0].elements["shippingCost"].value).replace(/,/g,'') * 1;
	  }
	  if (!isNaN(parseFloat(document.forms[0].elements["percentMarkup"].value))) {
	      percentMarkup = (document.forms[0].elements["percentMarkup"].value).replace(/,/g,'') * 1;
	  }
	  if (!isNaN(parseFloat(document.forms[0].elements["salesPrice"].value))) {
	      salesPrice = (document.forms[0].elements["salesPrice"].value).replace(/,/g,'') * 1;
	  }

	  averageCost = unitCost;
	  
  	  if (name == "percentMarkup") {
  	    	     
         salesPrice = ((averageCost + shippingCost) * percentMarkup / 100 + averageCost + shippingCost).toFixed(2);  	  	 
		 document.forms[0].elements["salesPrice"].value = salesPrice;
		 
	  } else if (name == "salesPrice" || name == "unitCost"  || name == "shippingCost") {
	  
	  	 document.forms[0].elements["percentMarkup"].value = ((salesPrice - averageCost - shippingCost) / (averageCost + shippingCost) * 100).toFixed(3);

	  }
	  
	  document.forms[0].elements["grossProfit"].value = (salesPrice - (averageCost + shippingCost)).toFixed(2);
	  
	  
}

//Done Hiding--> 
</script>

<link href="css/tabcontrol.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="js/tabber.js"></script>


<link href="css/tabber.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="js/tabber.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" onkeydown="return enterSubmit(event, new Array('saveButton'));">
<html:form action="/invItemEntry.do" onsubmit="return submitForm();">
  <%@ include file="cmnHeader.jsp" %> 
  <%@ include file="cmnSidebar.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="770" height="200">
  	<tr valign="top">
    	<td width="187" height="200"></td> 
        <td width="581" height="200">
        <table border="0" cellpadding="0" cellspacing="0" width="585" height="200" bgcolor="<%=Constants.TXN_MAIN_BGC%>">
	     <tr>
	        <td width="575" height="1" colspan="4" class="txnHeader" bgcolor="<%=Constants.TXN_HEADER_BGC%>">
		       <bean:message key="itemEntry.title"/>
		    </td>
	     </tr>
         <tr>
	        <td width="575" height="44" colspan="4" class="statusBar">
		    <logic:equal name="invItemEntryForm" property="txnStatus" value="<%=Constants.STATUS_SUCCESS%>">
               <bean:message key="app.success"/>
            </logic:equal>
		       <html:errors/>	
	        </td>
	 	 </tr>

		<html:hidden property="isUnitMeasureEntered" value=""/>
		<html:hidden property="isItemEntered" value=""/> 
		 <%-- full access --%>
		<logic:equal name="invItemEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
		<tr>
			<td>
				<div class="tabber">
				
				<div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     <tr> 
				        <td class="prompt" height="25" colspan="4">				        </td>
				     </tr>
				     
				     <tr>
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.itemName"/>
			           </td>
				        <td width="435" height="25" class="control" colspan="3">
				           <html:text property="itemName" size="43" maxlength="100" styleClass="textRequired"/>
						   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showInvItemLookup('itemName','','isItemEntered');"/>
			           </td>
					 </tr>
				   	 <tr>
		    			<td class="prompt" width="140" height="25">
			               <bean:message key="itemEntry.prompt.description"/>
	            	   </td>
					    <td width="435" height="25" class="control" colspan="3">
				       <html:text property="description" size="43" maxlength="100" styleClass="text"/>
				       </td>		    
					 </tr>         
			         
			         <tr>
				        <td class="prompt" width="140" height="25">
            			<bean:message key="itemEntry.prompt.category"/>
		               </td>
					    <td width="148" height="25" class="control" colspan="3">
			            <html:select property="category" styleClass="comboRequired">
		               <html:options property="categoryList"/>
			            </html:select>
				       </td>		    
					 </tr>
					 
					 <tr>
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.partNumber"/>
			           </td>
				        <td width="147" height="25" class="control" colspan="3">
				           <html:text property="partNumber" size="20" maxlength="25" styleClass="text"/>
			           </td>
					 </tr>
				     
				     <tr>
				        <td class="prompt" width="140" height="25">
			               <bean:message key="itemEntry.prompt.unitMeasure"/>
		               </td>
					    <td width="148" height="25" class="control">
					       <html:select property="unitMeasure" styleClass="comboRequired" onchange="return enterSelect('unitMeasure','isUnitMeasureEntered');">
					          <html:options property="unitMeasureList"/>		          
					       </html:select>
				       </td>
					    <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.unitCost"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="unitCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name); recalculatePrice(name);"/>
			           </td>		    
					 </tr>		  		  		 		  		  			 		 		 	 	 	 		  
					 
					 <tr> 
					    <td class="prompt" width="140" height="25">
			            <bean:message key="itemEntry.prompt.costMethod"/>
		               </td>
					    <td width="148" height="25" class="control">
			            <html:select property="costMethod" styleClass="comboRequired">
			               <html:options property="costMethodList"/>
			            </html:select>
				       </td>
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.averageCost"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="averageCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
			           </td>	        
				     </tr>
				     
				     <tr> 
					    <td class="prompt" width="140" height="25">
			            <bean:message key="itemEntry.prompt.percentMarkup"/>
		               </td>
					    <td width="148" height="25" class="control">
				            <html:text property="percentMarkup" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);recalculatePrice(name);"/>
				       </td>
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.shippingCost"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="shippingCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);recalculatePrice(name);"/>
			           </td>	        
				     </tr>
					 
					 <tr>	
						<td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.enable"/>
			           </td>		    		    		    		  
				        <td width="148" height="25" class="control">
				           <html:checkbox property="enable"/>
			           </td>	
					    <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.salesPrice"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="salesPrice" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);recalculatePrice(name);"/>
			           </td>	       		  	     
					 </tr>
					 
					 <tr>	
						
						<td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.enablePo"/>
			           </td>		    		    		    		  
				        <td width="435" height="25" class="control">
				           <html:checkbox property="enablePo"/>
			           </td>
			           	 
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.grossProfit"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="grossProfit" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
			           </td>	        		       		  	     
					 </tr>
					 
					  <tr>

					    
					    <td class="prompt" width="140" height="25">
			               <bean:message key="itemEntry.prompt.customer"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:select property="customer" styleClass="combo">
					          <html:options property="customerList"/>		          
					       </html:select>
					    </td>
					 </tr>
					
					 
					 <tr>
					 	<td class="prompt" width="140" height="25">
			               <bean:message key="itemEntry.prompt.supplier"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:select property="supplier" styleClass="combo">
					          <html:options property="supplierList"/>		          
					       </html:select>
					    </td>
					    
				
					 </tr>
					 
					 
				</table>
				</div>
				
			    <div class="tabbertab" title="Misc">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					<tr> 
						<td class="prompt" height="25" colspan="4"></td>
					</tr>						
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.doneness"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="doneness" styleClass="combo">
						<html:options property="donenessList"/>
						</html:select>
					  	</td>
					  	
					  	<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.virtualStore"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="virtualStore"/>
					  	</td>
					  	</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.sidings"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:text property="sidings" size="25" maxlength="50" styleClass="text"/>		
						</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.market"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:text property="market" size="10" maxlength="50" styleClass="text"/>		
					  	</td>  
					</tr>
					<tr>
						<td class="prompt" width="140" height="45">
						<bean:message key="itemEntry.prompt.remarks"/>
					  	</td>
						<td width="495" height="45" class="control" colspan="3">
						<html:textarea property="remarks" cols="45" rows="2" styleClass="text"/>
						</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.packaging"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="packaging" styleClass="combo">
					    <html:options property="packagingList"/>		          
					    </html:select>
					  	</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.poCycle"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:text property="poCycle" size="10" maxlength="50" styleClass="text"/>		
					  	</td>
					</tr> 
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.retailUom"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="retailUom" styleClass="combo">
					    <html:options property="retailUomList"/>		          
					    </html:select>
					  	</td>
					  	<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.defaultLocation"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:select property="defaultLocation" styleClass="combo">
					    <html:options property="defaultLocationList"/>		          
					    </html:select>
					  	</td>
					</tr> 
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.serviceCharge"/>
						</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="serviceCharge"/>
					  	</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.nonInventoriable"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:checkbox property="nonInventoriable"/>
					  	</td>
					</tr>
					
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.isVatRelief"/>
						</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="isVatRelief"/>
					  	</td>
						
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.openProduct"/>
						</td>
						<td width="148" height="25" class="control" colspan="3">
						<html:checkbox property="openProduct"/>
					  	</td>
					  	
					</tr>
					<tr>
						<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.taxCode"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:select property="taxCode" styleClass="combo">
					    <html:options property="taxCodeList"/>		          
					    </html:select>
					  	</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="itemEntry.prompt.fixedAsset"/>
						</td>
						<td width="148" height="25" class="control">
							<html:checkbox property="fixedAsset"/>
					  	</td>
            			<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.dateAcquired"/>
            			</td>
            			<td width="147" height="25" class="control">
               				<html:text property="dateAcquired" size="10" maxlength="10" styleClass="text"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
							<bean:message key="itemEntry.prompt.acquisitionCost"/>
						</td>
						<td width="147" height="25" class="control">
							<html:text property="acquisitionCost" size="10" maxlength="10" styleClass="text"/>
					  	</td>
            			<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.lifeSpan"/>
            			</td>
            			<td width="147" height="25" class="control">
               				<html:text property="lifeSpan" size="10" maxlength="10" styleClass="text"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
							<bean:message key="itemEntry.prompt.residualValue"/>
						</td>
						<td width="147" height="25" class="control">
							<html:text property="residualValue" size="10" maxlength="10" styleClass="text"/>
					  	</td>
				       <td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.condition"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:select property="condition" styleClass="combo">
					    <html:options property="conditionList"/>		          
					    </html:select>
					  	</td>
					</tr>
					<tr>
					
						<td width="130" height="25" class="prompt">
                   				<bean:message key="itemEntry.prompt.fixedAssetAccount"/>
                		</td>
                		<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="fixedAssetAccount" size="25" maxlength="255" />
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('fixedAssetAccount','fixedAssetAccountDescription');"/>	            
                		</td>
            			
					</tr>
					<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="itemEntry.prompt.fixedAssetAccountDescription"/>
               				</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="fixedAssetAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
                			</td>
         			</tr>          
					<tr>
					
						<td width="130" height="25" class="prompt">
                   				<bean:message key="itemEntry.prompt.depreciationAccount"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="depreciationAccount" size="25" maxlength="255" />
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('depreciationAccount','depreciationAccountDescription');"/>	            
                		</td>
					</tr>
					<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="itemEntry.prompt.depreciationAccountDescription"/>
               				</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="depreciationAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
                			</td>
         			</tr> 
         			<tr>
					
						<td width="130" height="25" class="prompt">
                   				<bean:message key="itemEntry.prompt.AccumulatedDepreciationAccount"/>
                			</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="accumulatedDepreciationAccount" size="25" maxlength="255" />
                   				<nested:image property="lookupButton" src="images/lookup.gif" onclick="return showGlCoaLookupTxn('accumulatedDepreciationAccount','accumulatedDepreciationAccountDescription');"/>	            
                		</td>
					</tr>
         			<tr>
                			<td width="130" height="25" class="prompt">
                   				<bean:message key="itemEntry.prompt.AccumulatedDepreciationAccountDescription"/>
               				</td>
                			<td width="445" height="25" class="control" colspan="3">
                   				<html:text property="accumulatedDepreciationAccountDescription" size="60" maxlength="255" style="font-size:8pt;" styleClass="text" disabled="true"/>
                			</td>
         			</tr>   
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.trackMisc"/>
            			</td>
            			<td width="147" height="25" class="control">
               				<html:checkbox property="trackMisc"/>
            			</td>
					</tr>
				  </table>
				</div>
				<div class="tabbertab" title="Schedule">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scSunday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scSunday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scMonday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scMonday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scTuesday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scTuesday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scWednesday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scWednesday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scThursday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scThursday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scFriday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scFriday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scSaturday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scSaturday"/>
            			</td>
					</tr>
				</table>
				</div>
				<!--  
				<div class="tabbertab" title="Upload Items">
				 
				 <table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr>
						<td class="prompt" width="160" height="25">
				          Upload CSV file
				       </td>
				       
				       <td width="628" height="25" class="control" colspan="3">
			              <html:file property="uploadFile" size="35" styleClass="textRequired"/>
			              
			           </td>
						
					</tr>
						
				 </table>
				 
				 
				 
				</div>
				
				!-->
				
				
				
				</div><script>tabberAutomatic(tabberOptions)</script>
			</td>
		</tr>
		 <%-- buttons --%>  
		<tr>
	     	<td width="575" height="50" colspan="4"> 
	          <div id="buttons">
	           <p align="right">		       
		       <logic:equal name="invItemEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
               <html:submit property="saveButton" styleClass="mainButton">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="priceLevelsButton" styleClass="mainButton" style="width: 100;">
		          <bean:message key="button.priceLevels"/>
		       </html:submit>
		       <html:submit property="conversionButton" styleClass="mainButtonMedium">
	              <bean:message key="button.conversion"/>
	           </html:submit>
	           <html:submit property="recalcButton" styleClass="mainButtonMedium" onclick="recalculatePrice('unitCost'); return false;">
	              <bean:message key="button.recalc"/>
	           </html:submit>
		       <logic:equal name="invItemEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	              <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	           <p align="right">				
		       <logic:equal name="invItemEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
               <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="priceLevelsButton" styleClass="mainButton" style="width: 100;" disabled="true">
		          <bean:message key="button.priceLevels"/>
		       </html:submit>
		       <html:submit property="conversionButton" styleClass="mainButtonMedium" disabled="true">
		          <bean:message key="button.conversion"/>
		       </html:submit>
		       <html:submit property="recalcButton" styleClass="mainButtonMedium" disabled="true">
	              <bean:message key="button.recalc"/>
	           </html:submit>
   		       <logic:equal name="invItemEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
		      </div>
            </td>
		 </tr>
	    </logic:equal>	 

		 <%-- special access --%>
		<logic:equal name="invItemEntryForm" property="userPermission" value="<%=Constants.SPECIAL%>">
		<tr>
			<td>
				<div class="tabber">
				
				<div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     <tr> 
				        <td class="prompt" height="25" colspan="4">				        </td>
				     </tr>						
				     <tr>
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.itemName"/>
			           </td>
				        <td width="435" height="25" class="control" colspan="3">
				           <html:text property="itemName" size="43" maxlength="100" styleClass="textRequired"/>		
			           	   <html:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookupGrid(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>
			           </td>
					 </tr>
				   	 <tr>
		    			<td class="prompt" width="140" height="25">
			               <bean:message key="itemEntry.prompt.description"/>
	            	   </td>
					    <td width="435" height="25" class="control" colspan="3">
				       <html:text property="description" size="43" maxlength="100" styleClass="text"/>
				       </td>		    
					 </tr>         
			         <tr>
				        <td class="prompt" width="140" height="25">
            			<bean:message key="itemEntry.prompt.category"/>
		               </td>
					    <td width="148" height="25" class="control">
			            <html:select property="category" styleClass="comboRequired">
		               <html:options property="categoryList"/>
			            </html:select>
				       </td>
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.partNumber"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="partNumber" size="20" maxlength="25" styleClass="text"/>
			           </td>		    
					 </tr>
				     <tr>
				        <td class="prompt" width="140" height="25">
			               <bean:message key="itemEntry.prompt.unitMeasure"/>
		               </td>
					    <td width="148" height="25" class="control">
					       <html:select property="unitMeasure" styleClass="comboRequired" onchange="return enterSelect('unitMeasure','isUnitMeasureEntered');">
					          <html:options property="unitMeasureList"/>		          
					       </html:select>
				       </td>
					    <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.unitCost"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="unitCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name); recalculatePrice(name);"/>
			           </td>		    
					 </tr>		  		  		 		  		  			 		 		 	 	 	 		  
					 <tr> 
					    <td class="prompt" width="140" height="25">
			            <bean:message key="itemEntry.prompt.costMethod"/>
		               </td>
					    <td width="148" height="25" class="control">
			            <html:select property="costMethod" styleClass="comboRequired">
			               <html:options property="costMethodList"/>
			            </html:select>
				       </td>
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.averageCost"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="averageCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
			           </td>	        
				     </tr>
				     <tr> 
					    <td class="prompt" width="140" height="25">
			            <bean:message key="itemEntry.prompt.percentMarkup"/>
		               </td>
					    <td width="148" height="25" class="control">
				            <html:text property="percentMarkup" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);recalculatePrice(name);"/>
				       </td>
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.shippingCost"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="shippingCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);recalculatePrice(name);"/>
			           </td>	        
				     </tr>
					 <tr>	
						<td class="prompt" width="140" height="25">
			               <bean:message key="itemEntry.prompt.supplier"/>
			            </td>
					    <td width="148" height="25" class="control">
					       <html:select property="supplier" styleClass="combo">
					          <html:options property="supplierList"/>		          
					       </html:select>
					    </td>	
					    <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.salesPrice"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="salesPrice" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);recalculatePrice(name);"/>
			           </td>	       		  	     
					 </tr>
					 <tr>	
						<td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.enable"/>
			           </td>		    		    		    		  
				        <td width="148" height="25" class="control">
				           <html:checkbox property="enable"/>
			           </td>	 
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.grossProfit"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="grossProfit" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
			           </td>	        		       		  	     
					 </tr>
					 <tr>	
						<td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.enablePo"/>
			           </td>		    		    		    		  
				        <td width="435" height="25" class="control">
				           <html:checkbox property="enablePo"/>
			           </td>
			          	        		       		  	     
					 </tr>
				</table>
				</div>
				
			    <div class="tabbertab" title="Misc">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					<tr> 
						<td class="prompt" height="25" colspan="4"></td>
					</tr>						
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.doneness"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="doneness" styleClass="combo">
						<html:options property="donenessList"/>
						</html:select>
					  	</td>
					  	
					  	<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.virtualStore"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="virtualStore"/>
					  	</td>
					  	</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.sidings"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:text property="sidings" size="25" maxlength="50" styleClass="text"/>		
						</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.market"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:text property="market" size="10" maxlength="50" styleClass="text"/>		
					  	</td>  
					</tr>
					<tr>
						<td class="prompt" width="140" height="45">
						<bean:message key="itemEntry.prompt.remarks"/>
					  	</td>
						<td width="495" height="45" class="control" colspan="3">
						<html:textarea property="remarks" cols="45" rows="2" styleClass="text"/>
						</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.packaging"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="packaging" styleClass="combo">
					    <html:options property="packagingList"/>		          
					    </html:select>
					  	</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.poCycle"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:text property="poCycle" size="10" maxlength="50" styleClass="text"/>		
					  	</td>
					</tr> 
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.retailUom"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="retailUom" styleClass="combo">
					    <html:options property="retailUomList"/>		          
					    </html:select>
					  	</td>
					  	<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.defaultLocation"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:select property="defaultLocation" styleClass="combo">
					    <html:options property="defaultLocationList"/>		          
					    </html:select>
					  	</td>
					</tr> 
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.serviceCharge"/>
						</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="serviceCharge"/>
					  	</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.nonInventoriable"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:checkbox property="nonInventoriable"/>
					  	</td>
					</tr>
					
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.isVatRelief"/>
						</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="isVatRelief"/>
					  	</td>
						
					</tr>
					
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.openProduct"/>
						</td>
						<td width="148" height="25" class="control" colspan="3">
						<html:checkbox property="openProduct"/>
					  	</td>
					</tr>
					<tr>
						<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.taxCode"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:select property="taxCode" styleClass="combo">
					    <html:options property="taxCodeList"/>		          
					    </html:select>
					  	</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
							<bean:message key="itemEntry.prompt.fixedAsset"/>
						</td>
						<td width="148" height="25" class="control">
							<html:checkbox property="fixedAsset"/>
					  	</td>
            			<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.dateAcquired"/>
            			</td>
            			<td width="147" height="25" class="control">
               				<html:text property="dateAcquired" size="10" maxlength="10" styleClass="text"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.trackMisc"/>
            			</td>
            			<td width="147" height="25" class="control">
               				<html:checkbox property="trackMisc"/>
            			</td>
					</tr>
				  </table>
				</div>
				<div class="tabbertab" title="Schedule">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scSunday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scSunday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scMonday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scMonday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scTuesday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scTuesday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scWednesday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scWednesday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scThursday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scThursday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scFriday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scFriday"/>
            			</td>
					</tr>
					<tr>
						<td width="200" height="25" class="prompt">
               				<bean:message key="itemEntry.prompt.scSaturday"/>
            			</td>
            			<td width="147" height="25" class="control" colspan="4">
               				<html:checkbox property="scSaturday"/>
            			</td>
					</tr>
				</table>
				</div>
				</div><script>tabberAutomatic(tabberOptions)</script>
			</td>
		</tr>
		 <%-- buttons --%>  
		<tr>
	     	<td width="575" height="50" colspan="4"> 
	          <div id="buttons">
	           <p align="right">		       
		       <logic:equal name="invItemEntryForm" property="userPermission" value="<%=Constants.SPECIAL%>">
               <html:submit property="saveButton" styleClass="mainButton">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="priceLevelsButton" styleClass="mainButton" style="width: 100;">
		          <bean:message key="button.priceLevels"/>
		       </html:submit>
		       <html:submit property="conversionButton" styleClass="mainButtonMedium">
	              <bean:message key="button.conversion"/>
	           </html:submit>
	           <html:submit property="recalcButton" styleClass="mainButtonMedium" disabled="true">
	              <bean:message key="button.recalc"/>
	           </html:submit>
		       <logic:equal name="invItemEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	              <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	           <p align="right">				
		       <logic:equal name="invItemEntryForm" property="userPermission" value="<%=Constants.SPECIAL%>">
               <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="priceLevelsButton" styleClass="mainButton" style="width: 100;" disabled="true">
		          <bean:message key="button.priceLevels"/>
		       </html:submit>
		       <html:submit property="conversionButton" styleClass="mainButtonMedium" disabled="true">
		          <bean:message key="button.conversion"/>
		       </html:submit>
		       <html:submit property="recalcButton" styleClass="mainButtonMedium" disabled="true">
	              <bean:message key="button.recalc"/>
	           </html:submit>
   		       <logic:equal name="invItemEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
		      </div>
            </td>
		 </tr>
	    </logic:equal>		
	     
		 <%-- query --%>			     	 
	    <logic:equal name="invItemEntryForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">	     
		<tr>
			<td>         
				<div class="tabber">
								 
				<div class="tabbertab" title="Header">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
				     <tr> 
				        <td class="prompt" height="25" colspan="4">				        </td>
				     </tr>						
				     <tr>
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.itemName"/>
			          </td>
				        <td width="435" height="25" class="control" colspan="3">
				           <html:text property="itemName" size="43" maxlength="50" styleClass="textRequired" disabled="true"/>
				           <html:image property="lookupButton" src="images/lookup.gif" onclick="return showInvIiLookupGrid(name, 'itemName', 'itemDescription', 'location', 'isItemEntered');"/>		
			           </td>
					 </tr>
				   	 <tr>
		    			<td class="prompt" width="140" height="25">
			               <bean:message key="itemEntry.prompt.description"/>
	            	   </td>
					    <td width="435" height="25" class="control" colspan="3">
				       <html:text property="description" size="43" maxlength="50" styleClass="text" disabled="true"/>
				       </td>		    
					 </tr>         
			         <tr>
				        <td class="prompt" width="140" height="25">
            			<bean:message key="itemEntry.prompt.category"/>
		               </td>
					    <td width="148" height="25" class="control">
			            <html:select property="category" styleClass="comboRequired" disabled="true">
		               <html:options property="categoryList"/>
			            </html:select>
				       </td>
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.partNumber"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="partNumber" size="20" maxlength="25" styleClass="text" disabled="true"/>
			           </td>		    
					 </tr>
				     <tr>
				        <td class="prompt" width="140" height="25">
			               <bean:message key="itemEntry.prompt.unitMeasure"/>
		               </td>
					    <td width="148" height="25" class="control">
					       <html:select property="unitMeasure" styleClass="comboRequired" disabled="true">
					          <html:options property="unitMeasureList"/>		          
					       </html:select>
				       </td>
					    <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.unitCost"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="unitCost" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
			           </td>		    
					 </tr>		  		  		 		  		  			 		 		 	 	 	 		  
					 <tr> 
					    <td class="prompt" width="140" height="25">
			            <bean:message key="itemEntry.prompt.costMethod"/>
		               </td>
					    <td width="148" height="25" class="control">
			            <html:select property="costMethod" styleClass="comboRequired" disabled="true">
			               <html:options property="costMethodList"/>
			            </html:select>
				       </td>
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.averageCost"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="averageCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
			           </td>	        
				     </tr>
				     <tr> 
					    <td class="prompt" width="140" height="25">
			            <bean:message key="itemEntry.prompt.percentMarkup"/>
		               </td>
					    <td width="148" height="25" class="control">
				            <html:text property="percentMarkup" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
				       </td>
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.shippingCost"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="shippingCost" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
			           </td>	        
				     </tr>
					 <tr>	    		       		  	     
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.enable"/>
			           </td>		    		    		    		  
				       <td width="148" height="25" class="control">
				           <html:checkbox property="enable" disabled="true"/>
			           </td>	
					    <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.salesPrice"/>
			           </td>
				        <td width="147" height="25" class="control">
				           <html:text property="salesPrice" size="20" maxlength="25" styleClass="textAmount" onkeyup="formatAmount(name, (event)?event:window.event);" onblur="addZeroes(name);" disabled="true"/>
			           </td>        		       		  	     
					 </tr>
					 <tr>	
							
							<td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.enablePo"/>
			           </td>		    		    		    		  
				        <td width="435" height="25" class="control">
				           <html:checkbox property="enablePo" disabled="true"/>
				           
			           </td>
				        <td class="prompt" width="140" height="25">
				           <bean:message key="itemEntry.prompt.grossProfit"/>
			           </td>		    		    		    		  
				        <td width="147" height="25" class="control">
				           <html:text property="grossProfit" size="20" maxlength="25" styleClass="textAmount" disabled="true"/>
			           </td>	        		       		  	     
					 </tr>
					
					 
				</table>
				</div>
				
				<div class="tabbertab" title="Misc">
				<table border="0" cellpadding="0" cellspacing="0" width="575" height="25">  
					<tr> 
						<td class="prompt" height="25" colspan="4"></td>
					</tr>						
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.doneness"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="doneness" styleClass="combo" disabled="true">
						<html:options property="donenessList"/>
						</html:select>
					  	</td>
					  	
					  	<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.virtualStore"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="virtualStore"/>
					  	</td>
					  	</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.sidings"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:text property="sidings" size="25" maxlength="50" styleClass="text" disabled="true"/>		
						</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.market"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:text property="market" size="10" maxlength="50" styleClass="text" disabled="true"/>		
					  	</td>  
					</tr>
					<tr>
						<td class="prompt" width="140" height="45">
						<bean:message key="assemblyItemEntry.prompt.remarks"/>
					  	</td>
						<td width="495" height="45" class="control" colspan="3">
						<html:textarea property="remarks" cols="45" rows="2" styleClass="text" disabled="true"/>
						</td>
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.packaging"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="packaging" styleClass="combo" disabled="true">
					    <html:options property="packagingList"/>		          
					    </html:select>
					  	</td>
						<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.poCycle"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:text property="poCycle" size="10" maxlength="50" styleClass="text" disabled="true"/>		
					  	</td>
					</tr> 
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.retailUom"/>
					  	</td>
						<td width="148" height="25" class="control">
						<html:select property="retailUom" styleClass="combo" disabled="true">
					    <html:options property="retailUomList"/>		          
					    </html:select>
					  	</td>
					  	<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.defaultLocation"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:select property="defaultLocation" styleClass="combo" disabled="true">
					    <html:options property="defaultLocationList"/>		          
					    </html:select>
					  	</td>
					</tr> 
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="assemblyItemEntry.prompt.serviceCharge"/>
						</td>
						<td width="148" height="25" class="control">
						<html:checkbox property="serviceCharge" disabled="true"/>
					  	</td>	        		       		  	     
						<td class="prompt" width="200" height="25">
						<bean:message key="assemblyItemEntry.prompt.nonInventoriable"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:checkbox property="nonInventoriable" disabled="true"/>
					  	</td>	        		       		  	     
					</tr>
					<tr>
						<td class="prompt" width="140" height="25">
						<bean:message key="itemEntry.prompt.openProduct"/>
						</td>
						<td width="148" height="25" class="control" colspan="3">
						<html:checkbox property="openProduct" disabled="true"/>
					  	</td>	               		  	     
					</tr>
					<tr>
						<td class="prompt" width="200" height="25">
						<bean:message key="itemEntry.prompt.taxCode"/>
					  	</td>
						<td width="147" height="25" class="control">
						<html:select property="taxCode" styleClass="combo" disabled="true">
					    <html:options property="taxCodeList"/>		          
					    </html:select>
					  	</td>	
					</tr>
					<tr>
						<td class="prompt" width="140" height="45">
						<bean:message key="assemblyItemEntry.prompt.remarks"/>
					  	</td>
						<td width="495" height="45" class="control" colspan="3">
						<html:textarea property="remarks" cols="45" rows="2" styleClass="text" disabled="true"/>
						</td>
					</tr>
				</table>
				</div>
				</div><script>tabberAutomatic(tabberOptions)</script>
			</td>
		</tr>
				 <%-- buttons --%>  
		<tr>
	     	<td width="575" height="50" colspan="4"> 
	          <div id="buttons">
	           <p align="right">		       
		       <logic:equal name="invItemEntryForm" property="userPermission" value="<%=Constants.FULL_ACCESS%>">
               <html:submit property="saveButton" styleClass="mainButton">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="priceLevelsButton" styleClass="mainButton" style="width: 100;">
		          <bean:message key="button.priceLevels"/>
		       </html:submit>
		       <html:submit property="conversionButton" styleClass="mainButtonMedium">
	              <bean:message key="button.conversion"/>
	           </html:submit>
	           <html:submit property="recalcButton" styleClass="mainButtonMedium" onclick="recalculatePrice('unitCost'); return false;">
	              <bean:message key="button.recalc"/>
	           </html:submit>
		       <logic:equal name="invItemEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" onclick="return confirmDelete();">
	              <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton">
		          <bean:message key="button.close"/>
		       </html:submit>
		      </div>
		      <div id="buttonsDisabled" style="display: none;">
	           <p align="right">				
		       <logic:equal name="invItemEntryForm" property="userPermission" value="<%=Constants.QUERY_ONLY%>">
               <html:submit property="saveButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.save"/>
		       </html:submit>
		       </logic:equal>
		       <html:submit property="priceLevelsButton" styleClass="mainButton" style="width: 100;" disabled="true">
		          <bean:message key="button.priceLevels"/>
		       </html:submit>
		       <html:submit property="conversionButton" styleClass="mainButtonMedium" disabled="true">
		          <bean:message key="button.conversion"/>
		       </html:submit>
		       <html:submit property="recalcButton" styleClass="mainButtonMedium" disabled="true">
	              <bean:message key="button.recalc"/>
	           </html:submit>
   		       <logic:equal name="invItemEntryForm" property="showDeleteButton" value="true">
			   <html:submit property="deleteButton" styleClass="mainButton" disabled="true">
	              <bean:message key="button.delete"/>
	           </html:submit>
	           </logic:equal>
		       <html:submit property="closeButton" styleClass="mainButton" disabled="true">
		          <bean:message key="button.close"/>
		       </html:submit>
		      </div>
            </td>
		 </tr>
     	</logic:equal>	     
	      

	     	 
   		
   		 <tr>
			<td width="575" height="10" colspan="4" bgcolor="<%=Constants.TXN_HEADER_BGC%>">	        
			</td>
		 </tr>
		</table>
    	</td>
	</tr>
  </table>
</html:form>
<script language=JavaScript type=text/javascript>
  <!--
     if(document.forms[0].elements["itemName"] != null &&
        document.forms[0].elements["itemName"].disabled == false)
        document.forms[0].elements["itemName"].focus()
	       // -->
</script>
</body>
</html>